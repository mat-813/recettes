---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "category": {
          "createdAt": "2023-04-07T14:07:42.078Z",
          "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
          "name": "Dessert",
          "updatedAt": "2023-04-07T14:07:42.078Z"
        },
        "recipesId": "d97dd78d-e708-4282-840a-1543d40899db"
      }
    ],
    "createdAt": "2023-05-07T15:24:01.328Z",
    "id": "d97dd78d-e708-4282-840a-1543d40899db",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:27:09.794Z",
          "id": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
          "name": "Œufs",
          "updatedAt": "2023-04-07T13:27:09.794Z"
        },
        "ingredientId": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
        "line": 0,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 40,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
          "name": "Eau",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
        "line": 1,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 200,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-05-07T15:26:53.129Z",
          "id": "7f4ba873-87f6-45ee-9aae-708b9b825920",
          "name": "Yaourt",
          "updatedAt": "2023-05-07T15:26:53.129Z"
        },
        "ingredientId": "7f4ba873-87f6-45ee-9aae-708b9b825920",
        "line": 2,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 200,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.687Z",
          "id": "88c50003-42e9-476c-ae04-a301fd3294a3",
          "name": "farine",
          "updatedAt": "2023-04-02T19:35:05.687Z"
        },
        "ingredientId": "88c50003-42e9-476c-ae04-a301fd3294a3",
        "line": 3,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-05-07T15:27:32.668Z",
          "id": "7a655b53-e3f1-446b-a00b-a79a8673feac",
          "name": "Sachet de poudre à lever",
          "updatedAt": "2023-05-07T15:27:32.668Z"
        },
        "ingredientId": "7a655b53-e3f1-446b-a00b-a79a8673feac",
        "line": 4,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 20,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.581Z",
          "id": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
          "name": "sucre",
          "updatedAt": "2023-04-02T19:35:05.581Z"
        },
        "ingredientId": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
        "line": 5,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      }
    ],
    "ingredientsSections": [
    ],
    "instructions": [
      {
        "instruction": "Fouetter les œufs et l'eau",
        "line": 0,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db"
      },
      {
        "instruction": "Ajouter le yaourt et mélanger",
        "line": 1,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db"
      },
      {
        "instruction": "Ajouter la farine, la poudre à lever et le sucre, et mélanger.",
        "line": 2,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db"
      },
      {
        "instruction": "Cuire à la poële",
        "line": 3,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db"
      },
      {
        "instruction": "<https://www.instagram.com/reel/CjGIG9pKCWd/>",
        "line": 4,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db"
      }
    ],
    "instructionsSections": [
      {
        "line": 4,
        "recipeId": "d97dd78d-e708-4282-840a-1543d40899db",
        "title": "Source"
      }
    ],
    "servings": 10,
    "servingsUnit": "pancakes",
    "slug": "pancakes",
    "title": "Pancake",
    "updatedAt": "2023-11-18T17:42:24.082Z"
  },
  "title": "Pancake"
}
---
