---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "category": {
          "createdAt": "2023-04-07T14:07:42.078Z",
          "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
          "name": "Dessert",
          "updatedAt": "2023-04-07T14:07:42.078Z"
        },
        "recipesId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      }
    ],
    "createdAt": "2023-04-09T15:23:02.955Z",
    "id": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 350,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T15:30:23.563Z",
          "id": "dd489242-cbec-486c-b40f-3d8c30e26bb8",
          "name": "raisins secs",
          "updatedAt": "2023-04-09T15:30:23.563Z"
        },
        "ingredientId": "dd489242-cbec-486c-b40f-3d8c30e26bb8",
        "line": 0,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 300,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:26:17.449Z",
          "id": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
          "name": "Beurre",
          "updatedAt": "2023-04-07T13:26:17.449Z"
        },
        "ingredientId": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
        "line": 1,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 300,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.687Z",
          "id": "88c50003-42e9-476c-ae04-a301fd3294a3",
          "name": "farine",
          "updatedAt": "2023-04-02T19:35:05.687Z"
        },
        "ingredientId": "88c50003-42e9-476c-ae04-a301fd3294a3",
        "line": 2,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 250,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.581Z",
          "id": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
          "name": "sucre",
          "updatedAt": "2023-04-02T19:35:05.581Z"
        },
        "ingredientId": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
        "line": 3,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 5,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:27:09.794Z",
          "id": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
          "name": "Œufs",
          "updatedAt": "2023-04-07T13:27:09.794Z"
        },
        "ingredientId": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
        "line": 4,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 200,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T15:34:24.490Z",
          "id": "766c1363-6458-4a4c-84a4-b1767fe3b01c",
          "name": "fruits confits",
          "updatedAt": "2023-04-09T15:34:24.490Z"
        },
        "ingredientId": "766c1363-6458-4a4c-84a4-b1767fe3b01c",
        "line": 5,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T15:34:39.866Z",
          "id": "f05a65ee-dc12-4c67-8724-ae738352b92e",
          "name": "rhum",
          "updatedAt": "2023-04-09T15:34:39.866Z"
        },
        "ingredientId": "f05a65ee-dc12-4c67-8724-ae738352b92e",
        "line": 6,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": {
          "createdAt": "2023-04-09T15:34:10.822Z",
          "factor": 0.02,
          "id": "c56e446f-894f-4e8b-ae3c-c989e9430273",
          "name": "verre à liqueur",
          "short": "vl",
          "type": "VOLUME",
          "updatedAt": "2023-04-09T15:34:10.822Z"
        },
        "unitId": "c56e446f-894f-4e8b-ae3c-c989e9430273"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.345Z",
          "id": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
          "name": "sel",
          "updatedAt": "2023-04-02T19:35:05.345Z"
        },
        "ingredientId": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
        "line": 7,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0003080575996875,
          "id": "91bae199-81fd-42b9-bde2-fb4044b9b4bb",
          "name": "pincée",
          "short": "pn",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "91bae199-81fd-42b9-bde2-fb4044b9b4bb"
      }
    ],
    "ingredientsSections": [
      {
        "line": 0,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "title": "Préparation"
      },
      {
        "line": 1,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "title": "Recette"
      }
    ],
    "instructions": [
      {
        "instruction": "Faire tremper le raisin dans le rhum (pendant des heures, ou, si vous avez peu de temps, faites chauffer le tout au micro-ondes).",
        "line": 0,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Préchauffer le four à 180 degrés.",
        "line": 1,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Dans un grand saladier, travailler le beurre en mousse.",
        "line": 2,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Ajouter 200 grammes de sucre et travailler pour faire blanchir et mousser le mélange.",
        "line": 3,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Séparer les blancs des jaunes et incorporer au mélange beurre sucre les jaunes, un à un pour bien les incorporer.",
        "line": 4,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Réserver les blancs dans un saladier.",
        "line": 5,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Incorporer la farine et le sel au mélange.",
        "line": 6,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Ajouter à l appareil les fruits confits égouttés et farinés.",
        "line": 7,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Ajouter le rhum et bien mélanger.",
        "line": 8,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Monter les blancs en neige et, pour les raffermir, ajouter avant de terminer, les 50 g de sucre restant.",
        "line": 9,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Délicatement, incorporer les blancs à la pâte en soulevant avec une cuiller en bois. (Le mieux est de commencer avec une petite partie des blancs, et d'ajouter le reste au fur et à mesure.)",
        "line": 10,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Verser dans un moule à cake rectangulaire préalablement beurré.",
        "line": 11,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Enfourner pour 60 minutes.",
        "line": 12,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Vérifier la cuisson avec la lame d un couteau.",
        "line": 13,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "instruction": "Un cake est toujours meilleur quand on a la patiente d'attendre un jour ou deux.",
        "line": 14,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      }
    ],
    "instructionsSections": [
      {
        "line": 0,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "title": "Préparation"
      },
      {
        "line": 2,
        "recipeId": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
        "title": "Recette"
      }
    ],
    "servings": 8,
    "servingsUnit": "Personnes",
    "slug": "cake",
    "title": "Cake",
    "updatedAt": "2023-11-18T17:39:37.286Z"
  },
  "title": "Cake"
}
---
