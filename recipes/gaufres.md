---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "category": {
          "createdAt": "2023-04-07T14:07:42.078Z",
          "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
          "name": "Dessert",
          "updatedAt": "2023-04-07T14:07:42.078Z"
        },
        "recipesId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed"
      }
    ],
    "createdAt": "2023-04-07T14:10:21.803Z",
    "id": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 500,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.687Z",
          "id": "88c50003-42e9-476c-ae04-a301fd3294a3",
          "name": "farine",
          "updatedAt": "2023-04-02T19:35:05.687Z"
        },
        "ingredientId": "88c50003-42e9-476c-ae04-a301fd3294a3",
        "line": 0,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 3,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:27:09.794Z",
          "id": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
          "name": "Œufs",
          "updatedAt": "2023-04-07T13:27:09.794Z"
        },
        "ingredientId": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
        "line": 1,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 75,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.581Z",
          "id": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
          "name": "sucre",
          "updatedAt": "2023-04-02T19:35:05.581Z"
        },
        "ingredientId": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
        "line": 2,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 15,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "bcd87ced-edee-4d20-a85a-c69356ccc2ff",
          "name": "Sucre vanillé",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "bcd87ced-edee-4d20-a85a-c69356ccc2ff",
        "line": 3,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 2,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
          "name": "Eau",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
        "line": 4,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 1,
          "id": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac",
          "name": "litre",
          "short": "l",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac"
      },
      {
        "amountdenum": 2,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-07T13:17:37.787Z",
          "id": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
          "name": "Lait",
          "updatedAt": "2023-04-07T13:17:37.787Z"
        },
        "ingredientId": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
        "line": 5,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 1,
          "id": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac",
          "name": "litre",
          "short": "l",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "f244f4fb-c5a6-4e7e-a3d7-80789f9692e9",
          "name": "Sachet de levure de boulanger",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "f244f4fb-c5a6-4e7e-a3d7-80789f9692e9",
        "line": 6,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 100,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:26:17.449Z",
          "id": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
          "name": "Beurre",
          "updatedAt": "2023-04-07T13:26:17.449Z"
        },
        "ingredientId": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
        "line": 7,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      }
    ],
    "ingredientsSections": [
    ],
    "instructions": [
      {
        "instruction": "Mélanger la farine, et les sucres dans une terrine.",
        "line": 0,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed"
      },
      {
        "instruction": "Ajouter jaunes d'oeufs, le lait et l'eau, puis le beurre fondu, et battre pendant au moins 5 minutes.",
        "line": 1,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed"
      },
      {
        "instruction": "Ajouter la levure.",
        "line": 2,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed"
      },
      {
        "instruction": "Incorporer les blancs battus en neige.",
        "line": 3,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed"
      }
    ],
    "instructionsSections": [
      {
        "line": 3,
        "recipeId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
        "title": "Après 30mn de repos"
      }
    ],
    "servings": 10,
    "servingsUnit": "Personnes",
    "slug": "gaufres",
    "title": "Gaufres",
    "updatedAt": "2023-11-18T17:42:16.885Z"
  },
  "title": "Gaufres"
}
---
