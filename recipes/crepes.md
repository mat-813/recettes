---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "category": {
          "createdAt": "2023-04-07T14:07:42.078Z",
          "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
          "name": "Dessert",
          "updatedAt": "2023-04-07T14:07:42.078Z"
        },
        "recipesId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      }
    ],
    "createdAt": "2023-04-07T13:02:53.109Z",
    "id": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
    "ingredientsRecipes": [
      {
        "amountdenum": 2,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-07T13:17:37.787Z",
          "id": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
          "name": "Lait",
          "updatedAt": "2023-04-07T13:17:37.787Z"
        },
        "ingredientId": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
        "line": 0,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 1,
          "id": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac",
          "name": "litre",
          "short": "l",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac"
      },
      {
        "amountdenum": 1,
        "amountint": 180,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.687Z",
          "id": "88c50003-42e9-476c-ae04-a301fd3294a3",
          "name": "farine",
          "updatedAt": "2023-04-02T19:35:05.687Z"
        },
        "ingredientId": "88c50003-42e9-476c-ae04-a301fd3294a3",
        "line": 1,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 30,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:21:20.545Z",
          "id": "38b21f76-fc05-4f56-a305-a2c1bcd87c7d",
          "name": "Maïzéna",
          "updatedAt": "2023-04-07T13:21:20.545Z"
        },
        "ingredientId": "38b21f76-fc05-4f56-a305-a2c1bcd87c7d",
        "line": 2,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 60,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:26:17.449Z",
          "id": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
          "name": "Beurre",
          "updatedAt": "2023-04-07T13:26:17.449Z"
        },
        "ingredientId": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
        "line": 3,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 4,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:27:09.794Z",
          "id": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
          "name": "Œufs",
          "updatedAt": "2023-04-07T13:27:09.794Z"
        },
        "ingredientId": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
        "line": 4,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 3,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.581Z",
          "id": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
          "name": "sucre",
          "updatedAt": "2023-04-02T19:35:05.581Z"
        },
        "ingredientId": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
        "line": 5,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 3,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-09T15:58:33.012Z",
          "id": "602b2a0c-d03e-459c-98e2-03cc99fb5b30",
          "name": "bière",
          "updatedAt": "2023-04-09T15:58:33.012Z"
        },
        "ingredientId": "602b2a0c-d03e-459c-98e2-03cc99fb5b30",
        "line": 6,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
        "unit": {
          "createdAt": "2023-04-09T15:55:16.166Z",
          "factor": 0.33,
          "id": "beb9a538-79e5-4e46-90fb-3ddff04e72a3",
          "name": "bouteille 33cl",
          "short": "bb",
          "type": "VOLUME",
          "updatedAt": "2023-04-09T15:55:16.166Z"
        },
        "unitId": "beb9a538-79e5-4e46-90fb-3ddff04e72a3"
      }
    ],
    "ingredientsSections": [
    ],
    "instructions": [
      {
        "instruction": "Faire fondre le beurre.",
        "line": 0,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "instruction": "Mettre la farine dans une terrine.",
        "line": 1,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "instruction": "Verser le lait en tournant avec une cuiller en bois de façon à faire une pâte liquide et sans grumeaux.",
        "line": 2,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "instruction": "Casser dans cette pâte les oeufs entiers, remuer bien pour que le mélange soit homogène.",
        "line": 3,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "instruction": "Ajouter le sucre.",
        "line": 4,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "instruction": "Mettre le beurre fondu dans la pâte, remuer à nouveau.",
        "line": 5,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "instruction": "Ajouter la bière.",
        "line": 6,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "instruction": "Laisser reposer plusieurs heures (min. 30 minutes).",
        "line": 7,
        "recipeId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      }
    ],
    "instructionsSections": [
    ],
    "servings": 10,
    "servingsUnit": "personnes",
    "slug": "crepes",
    "title": "Crêpes",
    "updatedAt": "2023-11-18T17:41:48.475Z"
  },
  "title": "Crêpes"
}
---
