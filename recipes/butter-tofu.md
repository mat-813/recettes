---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "8b0e03d3-8528-4244-86bd-29252531ce69",
        "category": {
          "createdAt": "2023-04-02T19:35:05.247Z",
          "id": "8b0e03d3-8528-4244-86bd-29252531ce69",
          "name": "Plat",
          "updatedAt": "2023-04-02T19:35:05.247Z"
        },
        "recipesId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "category": {
          "createdAt": "2023-04-02T19:35:05.283Z",
          "id": "9d50f268-8c2b-4d70-affc-466f711ba157",
          "name": "Vegan",
          "updatedAt": "2023-04-02T19:35:05.283Z"
        },
        "recipesId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      }
    ],
    "createdAt": "2023-04-02T20:10:02.330Z",
    "id": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 400,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.308Z",
          "id": "6da2319e-149b-405c-b3ff-0d64b1b44398",
          "name": "tofu ferme, épongé",
          "updatedAt": "2023-04-02T19:35:05.308Z"
        },
        "ingredientId": "6da2319e-149b-405c-b3ff-0d64b1b44398",
        "line": 0,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.330Z",
          "id": "f5190caa-4999-402b-aa10-d8c33a1d1eb0",
          "name": "fécule de maïs",
          "updatedAt": "2023-04-02T19:35:05.330Z"
        },
        "ingredientId": "f5190caa-4999-402b-aa10-d8c33a1d1eb0",
        "line": 1,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.345Z",
          "id": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
          "name": "sel",
          "updatedAt": "2023-04-02T19:35:05.345Z"
        },
        "ingredientId": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
        "line": 2,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.359Z",
          "id": "ac87ace7-5123-45fe-86d0-3e0faaac6ca9",
          "name": "cumin",
          "updatedAt": "2023-04-02T19:35:05.359Z"
        },
        "ingredientId": "ac87ace7-5123-45fe-86d0-3e0faaac6ca9",
        "line": 3,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.371Z",
          "id": "9b13f704-1af6-4f7d-903c-cbd18e237664",
          "name": "masalé",
          "updatedAt": "2023-04-02T19:35:05.371Z"
        },
        "ingredientId": "9b13f704-1af6-4f7d-903c-cbd18e237664",
        "line": 4,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 2,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.382Z",
          "id": "1fa1e800-b231-49a0-a412-5f47bb3d7bd3",
          "name": "curcuma",
          "updatedAt": "2023-04-02T19:35:05.382Z"
        },
        "ingredientId": "1fa1e800-b231-49a0-a412-5f47bb3d7bd3",
        "line": 5,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 2,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.394Z",
          "id": "96bcb3ef-f766-4ede-a066-37108935e56d",
          "name": "flocons de piment",
          "updatedAt": "2023-04-02T19:35:05.394Z"
        },
        "ingredientId": "96bcb3ef-f766-4ede-a066-37108935e56d",
        "line": 6,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 2,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.405Z",
          "id": "1f010bec-332d-4856-9684-5a65e6a7b700",
          "name": "poudre de gingembre",
          "updatedAt": "2023-04-02T19:35:05.405Z"
        },
        "ingredientId": "1f010bec-332d-4856-9684-5a65e6a7b700",
        "line": 7,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.417Z",
          "id": "6592a261-0d8f-4196-b807-c517656a370d",
          "name": "oignon",
          "updatedAt": "2023-04-02T19:35:05.417Z"
        },
        "ingredientId": "6592a261-0d8f-4196-b807-c517656a370d",
        "line": 8,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.429Z",
          "id": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
          "name": "gousses d'ail",
          "updatedAt": "2023-04-02T19:35:05.429Z"
        },
        "ingredientId": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
        "line": 9,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.441Z",
          "id": "255cf897-add7-4c8a-a8e3-d42e66946083",
          "name": "morceau de gingembre (taille d'un dé à coudre)",
          "updatedAt": "2023-04-02T19:35:05.441Z"
        },
        "ingredientId": "255cf897-add7-4c8a-a8e3-d42e66946083",
        "line": 10,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-10T06:20:37.124Z",
          "id": "87b90a9f-b0c1-4103-9564-5baeb79bd69b",
          "name": "huile ou beurre",
          "updatedAt": "2023-04-10T06:20:37.124Z"
        },
        "ingredientId": "87b90a9f-b0c1-4103-9564-5baeb79bd69b",
        "line": 11,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.465Z",
          "id": "f8e0b9db-7c5e-4b6d-8529-9f38a9d13b02",
          "name": "baton de canelle (ou 1cc)",
          "updatedAt": "2023-04-02T19:35:05.465Z"
        },
        "ingredientId": "f8e0b9db-7c5e-4b6d-8529-9f38a9d13b02",
        "line": 12,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.476Z",
          "id": "97998861-5c7c-4d01-a682-6e259d1a8d0f",
          "name": "graines de coriandre",
          "updatedAt": "2023-04-02T19:35:05.476Z"
        },
        "ingredientId": "97998861-5c7c-4d01-a682-6e259d1a8d0f",
        "line": 13,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.489Z",
          "id": "f95f7fac-3014-4257-88ff-e4f1197ff07e",
          "name": "concentré de tomate",
          "updatedAt": "2023-04-02T19:35:05.489Z"
        },
        "ingredientId": "f95f7fac-3014-4257-88ff-e4f1197ff07e",
        "line": 14,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 200,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.499Z",
          "id": "37e82692-1ce3-48d4-866d-ff0f876386b5",
          "name": "tomates concassées",
          "updatedAt": "2023-04-02T19:35:05.499Z"
        },
        "ingredientId": "37e82692-1ce3-48d4-866d-ff0f876386b5",
        "line": 15,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.359Z",
          "id": "ac87ace7-5123-45fe-86d0-3e0faaac6ca9",
          "name": "cumin",
          "updatedAt": "2023-04-02T19:35:05.359Z"
        },
        "ingredientId": "ac87ace7-5123-45fe-86d0-3e0faaac6ca9",
        "line": 16,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.371Z",
          "id": "9b13f704-1af6-4f7d-903c-cbd18e237664",
          "name": "masalé",
          "updatedAt": "2023-04-02T19:35:05.371Z"
        },
        "ingredientId": "9b13f704-1af6-4f7d-903c-cbd18e237664",
        "line": 17,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.394Z",
          "id": "96bcb3ef-f766-4ede-a066-37108935e56d",
          "name": "flocons de piment",
          "updatedAt": "2023-04-02T19:35:05.394Z"
        },
        "ingredientId": "96bcb3ef-f766-4ede-a066-37108935e56d",
        "line": 18,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 0,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.345Z",
          "id": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
          "name": "sel",
          "updatedAt": "2023-04-02T19:35:05.345Z"
        },
        "ingredientId": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
        "line": 19,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 125,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.555Z",
          "id": "7d8dab95-c0ce-4b88-bab9-a71996f75cca",
          "name": "lait de coco",
          "updatedAt": "2023-04-02T19:35:05.555Z"
        },
        "ingredientId": "7d8dab95-c0ce-4b88-bab9-a71996f75cca",
        "line": 20,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f",
          "name": "millilitre",
          "short": "ml",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-10T06:20:37.124Z",
          "id": "76ec5ea5-9f8c-4f1e-b4ba-eb54afad5804",
          "name": "jus de citron",
          "updatedAt": "2023-04-10T06:20:37.124Z"
        },
        "ingredientId": "76ec5ea5-9f8c-4f1e-b4ba-eb54afad5804",
        "line": 21,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.000616115199375,
          "id": "97248d94-5768-4c95-b2b7-857d80129cfe",
          "name": "soupçon",
          "short": "ds",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "97248d94-5768-4c95-b2b7-857d80129cfe"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.581Z",
          "id": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
          "name": "sucre",
          "updatedAt": "2023-04-02T19:35:05.581Z"
        },
        "ingredientId": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
        "line": 22,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      }
    ],
    "ingredientsSections": [
      {
        "line": 0,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Tofu"
      },
      {
        "line": 8,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Sauce 1ere partie"
      },
      {
        "line": 14,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Sauce 2nde partie"
      },
      {
        "line": 20,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Tout ensemble"
      }
    ],
    "instructions": [
      {
        "instruction": "Couper le tofu en \"bouchées\".",
        "line": 0,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Préchauffer le four à 175°C.",
        "line": 1,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Mélanger la fécule de maïs, le sel et les épices.",
        "line": 2,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Tremper le tofu dans les épices pour bien le recouvrir.",
        "line": 3,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Enfourner pendant 20mn.",
        "line": 4,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Émincer l'oignon. l'ail et le gingembre.",
        "line": 5,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Dans une sauteuse, mettre la matière grasse, le baton de canelle et les graines de coriandre.",
        "line": 6,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Ajouter l'oignon, l'ail et le gingembre.",
        "line": 7,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Laisser frire pendant quelques minutes.",
        "line": 8,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Ajouter le concentré de tomate et les épices, bien mélanger.",
        "line": 9,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Ajouter les tomates concassées, et laisser mijoter jusqu'à ce que ça sente bon partout.",
        "line": 10,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Ajouter le lait de coco, le citron et le sucre, bien mélanger.",
        "line": 11,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Ajouter le tofu sorti du four, et bien recouvrir les morceaux de tofu.",
        "line": 12,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "Servir avec du riz et des naans.",
        "line": 13,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "instruction": "<https://www.instagram.com/p/CgsIGYBqHKd/>",
        "line": 14,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      }
    ],
    "instructionsSections": [
      {
        "line": 0,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Tofu"
      },
      {
        "line": 5,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Sauce 1ere partie"
      },
      {
        "line": 9,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Sauce 2nde partie"
      },
      {
        "line": 11,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Tout ensemble"
      },
      {
        "line": 14,
        "recipeId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
        "title": "Source"
      }
    ],
    "servings": 2,
    "servingsUnit": "parts",
    "slug": "butter-tofu",
    "title": "Butter Tofu",
    "updatedAt": "2023-11-18T17:38:24.338Z"
  },
  "title": "Butter Tofu"
}
---
