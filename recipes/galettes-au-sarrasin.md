---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "category": {
          "createdAt": "2023-04-07T14:07:42.078Z",
          "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
          "name": "Dessert",
          "updatedAt": "2023-04-07T14:07:42.078Z"
        },
        "recipesId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      },
      {
        "categoriesId": "8b0e03d3-8528-4244-86bd-29252531ce69",
        "category": {
          "createdAt": "2023-04-02T19:35:05.247Z",
          "id": "8b0e03d3-8528-4244-86bd-29252531ce69",
          "name": "Plat",
          "updatedAt": "2023-04-02T19:35:05.247Z"
        },
        "recipesId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      }
    ],
    "createdAt": "2023-04-09T16:13:48.821Z",
    "id": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:27:09.794Z",
          "id": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
          "name": "Œufs",
          "updatedAt": "2023-04-07T13:27:09.794Z"
        },
        "ingredientId": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
        "line": 0,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 100,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T16:15:17.293Z",
          "id": "cc66ee6c-004f-449e-891e-7ad01c0ffaee",
          "name": "farine de sarrasin",
          "updatedAt": "2023-04-09T16:15:17.293Z"
        },
        "ingredientId": "cc66ee6c-004f-449e-891e-7ad01c0ffaee",
        "line": 1,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 150,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T16:13:47.469Z",
          "id": "24b11a63-ac3d-4b3b-9627-6a227be2bfeb",
          "name": "farine de froment",
          "updatedAt": "2023-04-09T16:13:47.469Z"
        },
        "ingredientId": "24b11a63-ac3d-4b3b-9627-6a227be2bfeb",
        "line": 2,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 4,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-07T13:17:37.787Z",
          "id": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
          "name": "Lait",
          "updatedAt": "2023-04-07T13:17:37.787Z"
        },
        "ingredientId": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
        "line": 3,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 1,
          "id": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac",
          "name": "litre",
          "short": "l",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac"
      },
      {
        "amountdenum": 4,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
          "name": "Eau",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
        "line": 4,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 1,
          "id": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac",
          "name": "litre",
          "short": "l",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.345Z",
          "id": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
          "name": "sel",
          "updatedAt": "2023-04-02T19:35:05.345Z"
        },
        "ingredientId": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
        "line": 5,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0003080575996875,
          "id": "91bae199-81fd-42b9-bde2-fb4044b9b4bb",
          "name": "pincée",
          "short": "pn",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "91bae199-81fd-42b9-bde2-fb4044b9b4bb"
      },
      {
        "amountdenum": 2,
        "amountint": 0,
        "amountnum": 1,
        "ingredient": {
          "createdAt": "2023-04-09T15:58:33.012Z",
          "id": "602b2a0c-d03e-459c-98e2-03cc99fb5b30",
          "name": "bière",
          "updatedAt": "2023-04-09T15:58:33.012Z"
        },
        "ingredientId": "602b2a0c-d03e-459c-98e2-03cc99fb5b30",
        "line": 6,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "unit": {
          "createdAt": "2023-04-09T15:55:16.166Z",
          "factor": 0.33,
          "id": "beb9a538-79e5-4e46-90fb-3ddff04e72a3",
          "name": "bouteille 33cl",
          "short": "bb",
          "type": "VOLUME",
          "updatedAt": "2023-04-09T15:55:16.166Z"
        },
        "unitId": "beb9a538-79e5-4e46-90fb-3ddff04e72a3"
      }
    ],
    "ingredientsSections": [
    ],
    "instructions": [
      {
        "instruction": "Mélanger le lait et l'eau, les farine et les oeufs.",
        "line": 0,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      },
      {
        "instruction": "Quand le mélange est homogène, ajouter la bière.",
        "line": 1,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      },
      {
        "instruction": "Laisser reposer une heures.",
        "line": 2,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      },
      {
        "instruction": "Ne pas laisser reposer trop sinon, les crêpes vont devenir trop légères (ou il sera difficile de les faire recuire après en y ajoutant une garniture).",
        "line": 3,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      },
      {
        "instruction": "Si la pâte est trop épaisse, ajouter de l'eau.",
        "line": 4,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      }
    ],
    "instructionsSections": [
      {
        "line": 3,
        "recipeId": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
        "title": "Remarques"
      }
    ],
    "servings": 6,
    "servingsUnit": "personnes",
    "slug": "galettes-au-sarrasin",
    "title": "Galettes au sarrasin",
    "updatedAt": "2023-11-18T17:42:10.895Z"
  },
  "title": "Galettes au sarrasin"
}
---
