---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
        "category": {
          "createdAt": "2023-11-18T11:31:39.186Z",
          "id": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
          "name": "Accompagnement",
          "updatedAt": "2023-11-18T11:31:39.186Z"
        },
        "recipesId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "category": {
          "createdAt": "2023-04-02T19:35:05.283Z",
          "id": "9d50f268-8c2b-4d70-affc-466f711ba157",
          "name": "Vegan",
          "updatedAt": "2023-04-02T19:35:05.283Z"
        },
        "recipesId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      }
    ],
    "createdAt": "2023-11-18T11:38:23.066Z",
    "id": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 400,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:38:23.066Z",
          "id": "4be34fc7-cc05-4dcb-b99f-6ce8b345738e",
          "name": "Tofu ferme",
          "updatedAt": "2023-11-18T11:38:23.066Z"
        },
        "ingredientId": "4be34fc7-cc05-4dcb-b99f-6ce8b345738e",
        "line": 0,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 3,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.429Z",
          "id": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
          "name": "gousses d'ail",
          "updatedAt": "2023-04-02T19:35:05.429Z"
        },
        "ingredientId": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
        "line": 1,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:42:05.568Z",
          "id": "2b752d9b-9af3-4794-a657-f0770a25722b",
          "name": "oignons nouveaux",
          "updatedAt": "2023-11-18T11:42:05.568Z"
        },
        "ingredientId": "2b752d9b-9af3-4794-a657-f0770a25722b",
        "line": 2,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.394Z",
          "id": "96bcb3ef-f766-4ede-a066-37108935e56d",
          "name": "flocons de piment",
          "updatedAt": "2023-04-02T19:35:05.394Z"
        },
        "ingredientId": "96bcb3ef-f766-4ede-a066-37108935e56d",
        "line": 3,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:42:05.568Z",
          "id": "9d8bb909-1279-4247-952e-7d37355a103b",
          "name": "huile de sésame",
          "updatedAt": "2023-11-18T11:42:05.568Z"
        },
        "ingredientId": "9d8bb909-1279-4247-952e-7d37355a103b",
        "line": 4,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 60,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:42:05.568Z",
          "id": "6c0f0250-d027-42be-9197-f6bcf24a0af6",
          "name": "sauce soja",
          "updatedAt": "2023-11-18T11:42:05.568Z"
        },
        "ingredientId": "6c0f0250-d027-42be-9197-f6bcf24a0af6",
        "line": 5,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f",
          "name": "millilitre",
          "short": "ml",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f"
      },
      {
        "amountdenum": 1,
        "amountint": 60,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
          "name": "Eau",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
        "line": 6,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f",
          "name": "millilitre",
          "short": "ml",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:43:49.467Z",
          "id": "96aa2c56-6086-4722-8ea4-a4a9cb0ccdc5",
          "name": "sirop d'agave ou sucre",
          "updatedAt": "2023-11-18T11:43:49.467Z"
        },
        "ingredientId": "96aa2c56-6086-4722-8ea4-a4a9cb0ccdc5",
        "line": 7,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      }
    ],
    "ingredientsSections": [
      {
        "line": 1,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "title": "Sauce"
      }
    ],
    "instructions": [
      {
        "instruction": "Couper le tofu en grosses tranches d'environ 1cm d'épaisseur.",
        "line": 0,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "instruction": "Faire frire dans de l'huile des deux côtés.",
        "line": 1,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "instruction": "Émincer l'ail et l'oignon.",
        "line": 2,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "instruction": "Ajouter le reste, et mixer.",
        "line": 3,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "instruction": "Couvrir le tofu avec la sauce.",
        "line": 4,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "instruction": "Laisser mijoter 10mn.",
        "line": 5,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "instruction": "[DUBU JORIM (두부조림)🇰🇷](https://www.instagram.com/reel/Cy4Dz5hKvzM/) par [@fitgreenmind](https://www.instagram.com/fitgreenmind/)",
        "line": 6,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      }
    ],
    "instructionsSections": [
      {
        "line": 2,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "title": "Sauce"
      },
      {
        "line": 6,
        "recipeId": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
        "title": "Source"
      }
    ],
    "servings": 3,
    "servingsUnit": "portions",
    "slug": "dubu-jorim",
    "title": "Dubu Jorim - 두부조림 🇰🇷 (tofu coréen)",
    "updatedAt": "2023-11-18T17:41:56.486Z"
  },
  "title": "Dubu Jorim - 두부조림 🇰🇷 (tofu coréen)"
}
---
