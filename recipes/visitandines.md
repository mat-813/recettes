---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "category": {
          "createdAt": "2023-04-07T14:07:42.078Z",
          "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
          "name": "Dessert",
          "updatedAt": "2023-04-07T14:07:42.078Z"
        },
        "recipesId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      }
    ],
    "createdAt": "2023-04-09T16:51:49.226Z",
    "id": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 150,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T16:51:49.226Z",
          "id": "7c75b1bd-0da4-4018-b1b3-4cc5375cb8b8",
          "name": "amandes en poudre",
          "updatedAt": "2023-04-09T16:51:49.226Z"
        },
        "ingredientId": "7c75b1bd-0da4-4018-b1b3-4cc5375cb8b8",
        "line": 0,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 200,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.581Z",
          "id": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
          "name": "sucre",
          "updatedAt": "2023-04-02T19:35:05.581Z"
        },
        "ingredientId": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
        "line": 1,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 8,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "bcd87ced-edee-4d20-a85a-c69356ccc2ff",
          "name": "Sucre vanillé",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "bcd87ced-edee-4d20-a85a-c69356ccc2ff",
        "line": 2,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 4,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T16:52:27.798Z",
          "id": "7b955b7a-d84b-43c8-ac45-406978008377",
          "name": "blancs d'œufs",
          "updatedAt": "2023-04-09T16:52:27.798Z"
        },
        "ingredientId": "7b955b7a-d84b-43c8-ac45-406978008377",
        "line": 3,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 75,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.687Z",
          "id": "88c50003-42e9-476c-ae04-a301fd3294a3",
          "name": "farine",
          "updatedAt": "2023-04-02T19:35:05.687Z"
        },
        "ingredientId": "88c50003-42e9-476c-ae04-a301fd3294a3",
        "line": 4,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 125,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:26:17.449Z",
          "id": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
          "name": "Beurre",
          "updatedAt": "2023-04-07T13:26:17.449Z"
        },
        "ingredientId": "0b260ad7-2756-4c3f-9e2b-89c695a346cd",
        "line": 5,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      }
    ],
    "ingredientsSections": [
    ],
    "instructions": [
      {
        "instruction": "Mélanger les amandes et les sucres.",
        "line": 0,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      },
      {
        "instruction": "Ajouter un à un les 4 blancs d'œufs (sans les fouetter).",
        "line": 1,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      },
      {
        "instruction": "Battre la pâte environ 10 minutes.",
        "line": 2,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      },
      {
        "instruction": "Ajouter la farine.",
        "line": 3,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      },
      {
        "instruction": "Faire cuire doucement le beurre, jusqu'à ce qu'il commence à brunir, ce que l'on appelle beurre noisette.",
        "line": 4,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      },
      {
        "instruction": "Ajouter le beurre refroidi.",
        "line": 5,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      },
      {
        "instruction": "Cuire la pâte dans un moule plat (à tarte par exemple) fariné, à 200°C et pendant 20 minutes.",
        "line": 6,
        "recipeId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      }
    ],
    "instructionsSections": [
    ],
    "servings": 6,
    "servingsUnit": "Personnes",
    "slug": "visitandines",
    "title": "Visitandines",
    "updatedAt": "2023-11-18T17:42:39.352Z"
  },
  "title": "Visitandines"
}
---
