---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
        "category": {
          "createdAt": "2023-11-18T11:31:39.186Z",
          "id": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
          "name": "Accompagnement",
          "updatedAt": "2023-11-18T11:31:39.186Z"
        },
        "recipesId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "category": {
          "createdAt": "2023-04-02T19:35:05.283Z",
          "id": "9d50f268-8c2b-4d70-affc-466f711ba157",
          "name": "Vegan",
          "updatedAt": "2023-04-02T19:35:05.283Z"
        },
        "recipesId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      }
    ],
    "createdAt": "2023-11-18T11:22:41.213Z",
    "id": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 3,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:22:41.213Z",
          "id": "c1a6d286-d17b-466d-97f5-6986def82378",
          "name": "gros poireaux",
          "updatedAt": "2023-11-18T11:22:41.213Z"
        },
        "ingredientId": "c1a6d286-d17b-466d-97f5-6986def82378",
        "line": 0,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:26:01.919Z",
          "id": "e64218df-e149-4729-aec0-7165647632aa",
          "name": "pâte miso",
          "updatedAt": "2023-11-18T11:26:01.919Z"
        },
        "ingredientId": "e64218df-e149-4729-aec0-7165647632aa",
        "line": 1,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.429Z",
          "id": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
          "name": "gousses d'ail",
          "updatedAt": "2023-04-02T19:35:05.429Z"
        },
        "ingredientId": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
        "line": 2,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T11:26:01.919Z",
          "id": "65316f7b-274d-4424-b044-1e304a57e06a",
          "name": "sirop d'agave ou de sucre",
          "updatedAt": "2023-11-18T11:26:01.919Z"
        },
        "ingredientId": "65316f7b-274d-4424-b044-1e304a57e06a",
        "line": 3,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-10T06:20:37.124Z",
          "id": "76ec5ea5-9f8c-4f1e-b4ba-eb54afad5804",
          "name": "jus de citron",
          "updatedAt": "2023-04-10T06:20:37.124Z"
        },
        "ingredientId": "76ec5ea5-9f8c-4f1e-b4ba-eb54afad5804",
        "line": 4,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 125,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
          "name": "Eau",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
        "line": 5,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f",
          "name": "millilitre",
          "short": "ml",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f"
      }
    ],
    "ingredientsSections": [
      {
        "line": 1,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "title": "Bouillon"
      }
    ],
    "instructions": [
      {
        "instruction": "Découper les poireaux en gros tronçons.",
        "line": 0,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "instruction": "Faire frire à la poêle dans un peu d'huile jusqu'à ce que les deux côtés soient un peu noircis",
        "line": 1,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "instruction": "Émincer l'ail.",
        "line": 2,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "instruction": "Mélanger tous les ingrédients du bouillon et mixer.",
        "line": 3,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "instruction": "Ajouter le bouillon sur les poireaux.",
        "line": 4,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "instruction": "Couvrir avec un couvercle et laisser cuire pendant 5-10mn jusqu' à ce que le bouillon ait bien réduit.",
        "line": 5,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "instruction": "<https://www.instagram.com/reel/CzKEjktqmE4/>",
        "line": 6,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      }
    ],
    "instructionsSections": [
      {
        "line": 2,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "title": "Bouillon"
      },
      {
        "line": 6,
        "recipeId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
        "title": "Source"
      }
    ],
    "servings": 3,
    "servingsUnit": "portions",
    "slug": "poireaux-au-miso",
    "title": "Poireaux au miso",
    "updatedAt": "2023-11-18T17:42:30.127Z"
  },
  "title": "Poireaux au miso"
}
---
