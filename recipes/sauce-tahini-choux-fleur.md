---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
        "category": {
          "createdAt": "2023-11-18T11:31:39.186Z",
          "id": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
          "name": "Accompagnement",
          "updatedAt": "2023-11-18T11:31:39.186Z"
        },
        "recipesId": "081b949b-bf14-438b-a6b1-df98ee3452e6"
      }
    ],
    "createdAt": "2024-03-11T12:39:03.221Z",
    "id": "081b949b-bf14-438b-a6b1-df98ee3452e6",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 3,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2024-03-11T12:39:03.221Z",
          "id": "c75d1cda-d561-4f2f-8139-7537a38c2b92",
          "name": "Tahini",
          "updatedAt": "2024-03-11T12:39:03.221Z"
        },
        "ingredientId": "c75d1cda-d561-4f2f-8139-7537a38c2b92",
        "line": 0,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2024-03-11T12:39:03.221Z",
          "id": "364b36c6-d863-4e2d-b2b5-0b702039e195",
          "name": "Sirop d'agave",
          "updatedAt": "2024-03-11T12:39:03.221Z"
        },
        "ingredientId": "364b36c6-d863-4e2d-b2b5-0b702039e195",
        "line": 1,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-10T06:20:37.124Z",
          "id": "76ec5ea5-9f8c-4f1e-b4ba-eb54afad5804",
          "name": "jus de citron",
          "updatedAt": "2023-04-10T06:20:37.124Z"
        },
        "ingredientId": "76ec5ea5-9f8c-4f1e-b4ba-eb54afad5804",
        "line": 2,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.725Z",
          "id": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
          "name": "huile d'olive",
          "updatedAt": "2023-04-02T19:35:05.725Z"
        },
        "ingredientId": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
        "line": 3,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2024-03-11T12:39:30.559Z",
          "id": "f6ea04bb-c529-4fb4-9856-c54590103f7d",
          "name": "Sriracha",
          "updatedAt": "2024-03-11T12:39:30.559Z"
        },
        "ingredientId": "f6ea04bb-c529-4fb4-9856-c54590103f7d",
        "line": 4,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.000616115199375,
          "id": "97248d94-5768-4c95-b2b7-857d80129cfe",
          "name": "soupçon",
          "short": "ds",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "97248d94-5768-4c95-b2b7-857d80129cfe"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
          "name": "Eau",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "f5831cdf-8226-419a-9cda-1ad6433d46b0",
        "line": 5,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.000616115199375,
          "id": "97248d94-5768-4c95-b2b7-857d80129cfe",
          "name": "soupçon",
          "short": "ds",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "97248d94-5768-4c95-b2b7-857d80129cfe"
      }
    ],
    "ingredientsSections": [
    ],
    "instructions": [
      {
        "instruction": "Mélanger les ingrédients",
        "line": 0,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6"
      },
      {
        "instruction": "Ajouter de l'eau pour rendre plus fluide",
        "line": 1,
        "recipeId": "081b949b-bf14-438b-a6b1-df98ee3452e6"
      }
    ],
    "instructionsSections": [
    ],
    "servings": 4,
    "servingsUnit": "personnes",
    "slug": "sauce-tahini-choux-fleur",
    "title": "Sauce tahini",
    "updatedAt": "2024-03-11T12:39:03.221Z"
  },
  "title": "Sauce tahini"
}
---
