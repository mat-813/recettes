---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "category": {
          "createdAt": "2023-04-07T14:07:42.078Z",
          "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
          "name": "Dessert",
          "updatedAt": "2023-04-07T14:07:42.078Z"
        },
        "recipesId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      }
    ],
    "createdAt": "2023-04-09T15:43:15.160Z",
    "id": "85bef96f-d926-405c-8443-81e5757e6d6a",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:17:37.787Z",
          "id": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
          "name": "Lait",
          "updatedAt": "2023-04-07T13:17:37.787Z"
        },
        "ingredientId": "3a74a8ab-107f-4aaf-b90c-97047d07ba70",
        "line": 0,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 1,
          "id": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac",
          "name": "litre",
          "short": "l",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "72a57e48-a606-4b5f-b1a6-20eb84b3a2ac"
      },
      {
        "amountdenum": 1,
        "amountint": 100,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.581Z",
          "id": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
          "name": "sucre",
          "updatedAt": "2023-04-02T19:35:05.581Z"
        },
        "ingredientId": "8d1526ba-a065-4d9a-8143-b4abff04f8af",
        "line": 1,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 8,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T14:11:45.983Z",
          "id": "bcd87ced-edee-4d20-a85a-c69356ccc2ff",
          "name": "Sucre vanillé",
          "updatedAt": "2023-04-07T14:11:45.983Z"
        },
        "ingredientId": "bcd87ced-edee-4d20-a85a-c69356ccc2ff",
        "line": 2,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 6,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-07T13:27:09.794Z",
          "id": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
          "name": "Œufs",
          "updatedAt": "2023-04-07T13:27:09.794Z"
        },
        "ingredientId": "c947677f-d89c-4aec-afbe-1c5d09aa8c4c",
        "line": 3,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-09T16:07:02.245Z",
          "id": "b3139336-b8d5-4a15-9199-25061847d09c",
          "name": "jaune d'œufs",
          "updatedAt": "2023-04-09T16:07:02.245Z"
        },
        "ingredientId": "b3139336-b8d5-4a15-9199-25061847d09c",
        "line": 4,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a",
        "unit": null,
        "unitId": null
      }
    ],
    "ingredientsSections": [
    ],
    "instructions": [
      {
        "instruction": "Faire bouillir le lait avec le sucre et le sucre vanillé (ou la gousse de vanille).",
        "line": 0,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      },
      {
        "instruction": "Laisser refroidir (retirer la gousse de vanille).",
        "line": 1,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      },
      {
        "instruction": "Battre les oeufs avec les jaunes.",
        "line": 2,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      },
      {
        "instruction": "Mélanger le tout au batteur.",
        "line": 3,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      },
      {
        "instruction": "Faire le caramel dans le moule, et y verser le mélange.",
        "line": 4,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      },
      {
        "instruction": "Cuire au thermostat 4 jusqu'à ce que le mélange soit consistant.",
        "line": 5,
        "recipeId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      }
    ],
    "instructionsSections": [
    ],
    "servings": 8,
    "servingsUnit": "Personnes",
    "slug": "creme-caramel",
    "title": "Crême caramel",
    "updatedAt": "2023-11-18T17:39:46.398Z"
  },
  "title": "Crême caramel"
}
---
