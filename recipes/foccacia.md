---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "d49501e4-268b-4ea7-a0aa-a98454cea496",
        "category": {
          "createdAt": "2023-04-02T19:35:05.672Z",
          "id": "d49501e4-268b-4ea7-a0aa-a98454cea496",
          "name": "Pain",
          "updatedAt": "2023-04-02T19:35:05.672Z"
        },
        "recipesId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "category": {
          "createdAt": "2023-04-02T19:35:05.283Z",
          "id": "9d50f268-8c2b-4d70-affc-466f711ba157",
          "name": "Vegan",
          "updatedAt": "2023-04-02T19:35:05.283Z"
        },
        "recipesId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      }
    ],
    "createdAt": "2023-04-02T20:10:02.649Z",
    "id": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 515,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.687Z",
          "id": "88c50003-42e9-476c-ae04-a301fd3294a3",
          "name": "farine",
          "updatedAt": "2023-04-02T19:35:05.687Z"
        },
        "ingredientId": "88c50003-42e9-476c-ae04-a301fd3294a3",
        "line": 0,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.698Z",
          "id": "cac0f92c-b71b-4dba-872a-e0d178ade99f",
          "name": "levure instantanée",
          "updatedAt": "2023-04-02T19:35:05.698Z"
        },
        "ingredientId": "cac0f92c-b71b-4dba-872a-e0d178ade99f",
        "line": 1,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.345Z",
          "id": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
          "name": "sel",
          "updatedAt": "2023-04-02T19:35:05.345Z"
        },
        "ingredientId": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
        "line": 2,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.004928921595,
          "id": "7eaaa96c-2e17-4c4d-be90-947e446babd0",
          "name": "cuillère à café",
          "short": "ts",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "7eaaa96c-2e17-4c4d-be90-947e446babd0"
      },
      {
        "amountdenum": 1,
        "amountint": 455,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.714Z",
          "id": "9dc796a2-92e9-4a44-bba3-dd67cd98eb7b",
          "name": "eau tiède",
          "updatedAt": "2023-04-02T19:35:05.714Z"
        },
        "ingredientId": "9dc796a2-92e9-4a44-bba3-dd67cd98eb7b",
        "line": 3,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f",
          "name": "millilitre",
          "short": "ml",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "66ff9d0b-9101-45dc-a238-43c83f1f6d2f"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.725Z",
          "id": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
          "name": "huile d'olive",
          "updatedAt": "2023-04-02T19:35:05.725Z"
        },
        "ingredientId": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
        "line": 4,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 3,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.725Z",
          "id": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
          "name": "huile d'olive",
          "updatedAt": "2023-04-02T19:35:05.725Z"
        },
        "ingredientId": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
        "line": 5,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.345Z",
          "id": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
          "name": "sel",
          "updatedAt": "2023-04-02T19:35:05.345Z"
        },
        "ingredientId": "92886e38-89de-43fc-ba1e-ea9d4ce219b0",
        "line": 6,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0003080575996875,
          "id": "91bae199-81fd-42b9-bde2-fb4044b9b4bb",
          "name": "pincée",
          "short": "pn",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "91bae199-81fd-42b9-bde2-fb4044b9b4bb"
      }
    ],
    "ingredientsSections": [
      {
        "line": 5,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "title": "Avant cuisson"
      }
    ],
    "instructions": [
      {
        "instruction": "Dans un grand saladier, mélanger la farine, la levure et le sel.",
        "line": 0,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Ajouter l'eau.",
        "line": 1,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Travailler avec une spatule jusqu'à ce que la pate soit uniforme et élastique.",
        "line": 2,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Mettre l'huile d'olive dessus et bien répartir sur la surface.",
        "line": 3,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Réfrigérer une nuit.",
        "line": 4,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Huiler un grand plat (ou mettre du papier sulfurisé)",
        "line": 5,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Y mettre la pate.",
        "line": 6,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Laisser reposer 3 à 4 heures.",
        "line": 7,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Préchauffer le four à 220°C",
        "line": 8,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Mettre l'huile d'olive et bien répartir à la main.",
        "line": 9,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Saler.",
        "line": 10,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Ajouter des ingrédients en plus, romarin, tomates cerise...",
        "line": 11,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "Enfourner pour 25-35 minutes.",
        "line": 12,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "instruction": "<https://www.instagram.com/p/CeHYVU8gJXe/>",
        "line": 13,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      }
    ],
    "instructionsSections": [
      {
        "line": 5,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "title": "Lendemain matin"
      },
      {
        "line": 8,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "title": "Avant cuisson"
      },
      {
        "line": 13,
        "recipeId": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
        "title": "Source"
      }
    ],
    "servings": 12,
    "servingsUnit": "morceaux",
    "slug": "foccacia",
    "title": "Foccacia",
    "updatedAt": "2023-11-18T17:42:04.441Z"
  },
  "title": "Foccacia"
}
---
