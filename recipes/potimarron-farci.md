---json
{
  "layout": "recipe",
  "recipe": {
    "categoriesRecipes": [
      {
        "categoriesId": "8b0e03d3-8528-4244-86bd-29252531ce69",
        "category": {
          "createdAt": "2023-04-02T19:35:05.247Z",
          "id": "8b0e03d3-8528-4244-86bd-29252531ce69",
          "name": "Plat",
          "updatedAt": "2023-04-02T19:35:05.247Z"
        },
        "recipesId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      }
    ],
    "createdAt": "2023-11-18T12:03:45.163Z",
    "id": "3ff395ea-2139-4d23-8de2-39fa538c7193",
    "ingredientsRecipes": [
      {
        "amountdenum": 1,
        "amountint": 800,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T12:12:50.073Z",
          "id": "32e5bd41-8f6e-493d-a3f2-60dea0fcb886",
          "name": "Potimarrons",
          "updatedAt": "2023-11-18T12:12:50.073Z"
        },
        "ingredientId": "32e5bd41-8f6e-493d-a3f2-60dea0fcb886",
        "line": 0,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 2,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.429Z",
          "id": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
          "name": "gousses d'ail",
          "updatedAt": "2023-04-02T19:35:05.429Z"
        },
        "ingredientId": "0c4ea9d2-5e93-48ad-8bfe-8280030c0a1f",
        "line": 1,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.417Z",
          "id": "6592a261-0d8f-4196-b807-c517656a370d",
          "name": "oignon",
          "updatedAt": "2023-04-02T19:35:05.417Z"
        },
        "ingredientId": "6592a261-0d8f-4196-b807-c517656a370d",
        "line": 2,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": null,
        "unitId": null
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T12:03:45.163Z",
          "id": "07381b64-ece2-4242-be83-a63ab390d2cb",
          "name": "Paprika",
          "updatedAt": "2023-11-18T12:03:45.163Z"
        },
        "ingredientId": "07381b64-ece2-4242-be83-a63ab390d2cb",
        "line": 3,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T12:03:45.163Z",
          "id": "af484735-d8cf-47ac-a5b7-f1b951f74321",
          "name": "Piment doux",
          "updatedAt": "2023-11-18T12:03:45.163Z"
        },
        "ingredientId": "af484735-d8cf-47ac-a5b7-f1b951f74321",
        "line": 4,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 1,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-04-02T19:35:05.725Z",
          "id": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
          "name": "huile d'olive",
          "updatedAt": "2023-04-02T19:35:05.725Z"
        },
        "ingredientId": "1a4fd542-49e3-454a-bfe3-b991c5e0809c",
        "line": 5,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.0147867647825,
          "id": "954261cd-f70e-43f3-8328-e3d2225c0463",
          "name": "cuillère à soupe",
          "short": "tb",
          "type": "VOLUME",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "954261cd-f70e-43f3-8328-e3d2225c0463"
      },
      {
        "amountdenum": 1,
        "amountint": 200,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T12:03:45.163Z",
          "id": "cc77fdce-cda5-45f3-9ef9-3cba25294dcc",
          "name": "Champignons",
          "updatedAt": "2023-11-18T12:03:45.163Z"
        },
        "ingredientId": "cc77fdce-cda5-45f3-9ef9-3cba25294dcc",
        "line": 6,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 100,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T12:06:54.809Z",
          "id": "ab2b4969-b229-4dd0-bed4-94458541051c",
          "name": "fromage de chèvre",
          "updatedAt": "2023-11-18T12:06:54.809Z"
        },
        "ingredientId": "ab2b4969-b229-4dd0-bed4-94458541051c",
        "line": 7,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      },
      {
        "amountdenum": 1,
        "amountint": 50,
        "amountnum": 0,
        "ingredient": {
          "createdAt": "2023-11-18T12:08:12.741Z",
          "id": "e0212b79-356a-4117-9a9e-dde498617cf1",
          "name": "Émmental râpé",
          "updatedAt": "2023-11-18T12:08:12.741Z"
        },
        "ingredientId": "e0212b79-356a-4117-9a9e-dde498617cf1",
        "line": 8,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "unit": {
          "createdAt": "2023-04-02T19:34:38.192Z",
          "factor": 0.001,
          "id": "59a2d155-9c62-444b-8300-e5dcf5ef0680",
          "name": "gramme",
          "short": "g",
          "type": "WEIGHT",
          "updatedAt": "2023-04-02T19:34:38.192Z"
        },
        "unitId": "59a2d155-9c62-444b-8300-e5dcf5ef0680"
      }
    ],
    "ingredientsSections": [
      {
        "line": 1,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "title": "Farce"
      }
    ],
    "instructions": [
      {
        "instruction": "Préchauffer le four à 200°C.",
        "line": 0,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Décapiter les potimarrons.",
        "line": 1,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Enlever les graines et la partie filandreuse.",
        "line": 2,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Émincer l'ail, l'oignon.",
        "line": 3,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Mélanger l'oignon, l'ail, et les épices avec l'huile.",
        "line": 4,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Couper les champignons et le fromage de chèvre en petits dés.",
        "line": 5,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Ajouter les champignons et le chèvre à la farce.",
        "line": 6,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Farcir les potimarrons, ne pas hésiter à bien tasser, les champignons vont réduire à la cuisson.",
        "line": 7,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Couvrir avec l'emmental pour gratiner.",
        "line": 8,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      },
      {
        "instruction": "Enfourner 45mn si il s'agit de 2 petits potimarrons ou 1h15 si c'est un seul gros.",
        "line": 9,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      }
    ],
    "instructionsSections": [
      {
        "line": 3,
        "recipeId": "3ff395ea-2139-4d23-8de2-39fa538c7193",
        "title": "Farce"
      }
    ],
    "servings": 3,
    "servingsUnit": "personnes",
    "slug": "potimarron-farci",
    "title": "Potimarron farci",
    "updatedAt": "2023-11-18T17:42:35.322Z"
  },
  "title": "Potimarron farci"
}
---
