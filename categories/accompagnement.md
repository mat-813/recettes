---json
{
  "category": {
    "categoriesRecipes": [
      {
        "categoriesId": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
        "recipe": {
          "createdAt": "2023-11-18T11:38:23.066Z",
          "id": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
          "servings": 3,
          "servingsUnit": "portions",
          "slug": "dubu-jorim",
          "title": "Dubu Jorim - 두부조림 🇰🇷 (tofu coréen)",
          "updatedAt": "2023-11-18T17:41:56.486Z"
        },
        "recipesId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "categoriesId": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
        "recipe": {
          "createdAt": "2023-11-18T11:22:41.213Z",
          "id": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
          "servings": 3,
          "servingsUnit": "portions",
          "slug": "poireaux-au-miso",
          "title": "Poireaux au miso",
          "updatedAt": "2023-11-18T17:42:30.127Z"
        },
        "recipesId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      },
      {
        "categoriesId": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
        "recipe": {
          "createdAt": "2024-03-11T12:39:03.221Z",
          "id": "081b949b-bf14-438b-a6b1-df98ee3452e6",
          "servings": 4,
          "servingsUnit": "personnes",
          "slug": "sauce-tahini-choux-fleur",
          "title": "Sauce tahini",
          "updatedAt": "2024-03-11T12:39:03.221Z"
        },
        "recipesId": "081b949b-bf14-438b-a6b1-df98ee3452e6"
      }
    ],
    "createdAt": "2023-11-18T11:31:39.186Z",
    "id": "07b25668-c599-4b76-bb14-fce7bd86bbe9",
    "name": "Accompagnement",
    "updatedAt": "2023-11-18T11:31:39.186Z"
  },
  "layout": "category",
  "title": "Accompagnement"
}
---
