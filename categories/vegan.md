---json
{
  "category": {
    "categoriesRecipes": [
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "recipe": {
          "createdAt": "2023-04-02T20:10:02.330Z",
          "id": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
          "servings": 2,
          "servingsUnit": "parts",
          "slug": "butter-tofu",
          "title": "Butter Tofu",
          "updatedAt": "2023-11-18T17:38:24.338Z"
        },
        "recipesId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "recipe": {
          "createdAt": "2023-11-18T11:38:23.066Z",
          "id": "2b1c134b-2234-47e1-b954-c3651a44b6e8",
          "servings": 3,
          "servingsUnit": "portions",
          "slug": "dubu-jorim",
          "title": "Dubu Jorim - 두부조림 🇰🇷 (tofu coréen)",
          "updatedAt": "2023-11-18T17:41:56.486Z"
        },
        "recipesId": "2b1c134b-2234-47e1-b954-c3651a44b6e8"
      },
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "recipe": {
          "createdAt": "2023-04-02T20:10:02.649Z",
          "id": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
          "servings": 12,
          "servingsUnit": "morceaux",
          "slug": "foccacia",
          "title": "Foccacia",
          "updatedAt": "2023-11-18T17:42:04.441Z"
        },
        "recipesId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      },
      {
        "categoriesId": "9d50f268-8c2b-4d70-affc-466f711ba157",
        "recipe": {
          "createdAt": "2023-11-18T11:22:41.213Z",
          "id": "7b4a47d1-978a-4805-b5b8-b76149a78f3f",
          "servings": 3,
          "servingsUnit": "portions",
          "slug": "poireaux-au-miso",
          "title": "Poireaux au miso",
          "updatedAt": "2023-11-18T17:42:30.127Z"
        },
        "recipesId": "7b4a47d1-978a-4805-b5b8-b76149a78f3f"
      }
    ],
    "createdAt": "2023-04-02T19:35:05.283Z",
    "id": "9d50f268-8c2b-4d70-affc-466f711ba157",
    "name": "Vegan",
    "updatedAt": "2023-04-02T19:35:05.283Z"
  },
  "layout": "category",
  "title": "Vegan"
}
---
