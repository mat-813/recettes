---json
{
  "category": {
    "categoriesRecipes": [
      {
        "categoriesId": "d49501e4-268b-4ea7-a0aa-a98454cea496",
        "recipe": {
          "createdAt": "2023-04-02T20:10:02.649Z",
          "id": "a738d314-3ed4-40ad-a51c-88b9f77981d3",
          "servings": 12,
          "servingsUnit": "morceaux",
          "slug": "foccacia",
          "title": "Foccacia",
          "updatedAt": "2023-11-18T17:42:04.441Z"
        },
        "recipesId": "a738d314-3ed4-40ad-a51c-88b9f77981d3"
      }
    ],
    "createdAt": "2023-04-02T19:35:05.672Z",
    "id": "d49501e4-268b-4ea7-a0aa-a98454cea496",
    "name": "Pain",
    "updatedAt": "2023-04-02T19:35:05.672Z"
  },
  "layout": "category",
  "title": "Pain"
}
---
