---json
{
  "category": {
    "categoriesRecipes": [
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "recipe": {
          "createdAt": "2023-04-09T15:23:02.955Z",
          "id": "c37f6249-8de2-41db-a21a-65a1f9776dc5",
          "servings": 8,
          "servingsUnit": "Personnes",
          "slug": "cake",
          "title": "Cake",
          "updatedAt": "2023-11-18T17:39:37.286Z"
        },
        "recipesId": "c37f6249-8de2-41db-a21a-65a1f9776dc5"
      },
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "recipe": {
          "createdAt": "2023-04-09T15:43:15.160Z",
          "id": "85bef96f-d926-405c-8443-81e5757e6d6a",
          "servings": 8,
          "servingsUnit": "Personnes",
          "slug": "creme-caramel",
          "title": "Crême caramel",
          "updatedAt": "2023-11-18T17:39:46.398Z"
        },
        "recipesId": "85bef96f-d926-405c-8443-81e5757e6d6a"
      },
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "recipe": {
          "createdAt": "2023-04-07T13:02:53.109Z",
          "id": "af2b243d-4a28-4e92-aa9a-9a39cea47957",
          "servings": 10,
          "servingsUnit": "personnes",
          "slug": "crepes",
          "title": "Crêpes",
          "updatedAt": "2023-11-18T17:41:48.475Z"
        },
        "recipesId": "af2b243d-4a28-4e92-aa9a-9a39cea47957"
      },
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "recipe": {
          "createdAt": "2023-04-09T16:13:48.821Z",
          "id": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
          "servings": 6,
          "servingsUnit": "personnes",
          "slug": "galettes-au-sarrasin",
          "title": "Galettes au sarrasin",
          "updatedAt": "2023-11-18T17:42:10.895Z"
        },
        "recipesId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      },
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "recipe": {
          "createdAt": "2023-04-07T14:10:21.803Z",
          "id": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed",
          "servings": 10,
          "servingsUnit": "Personnes",
          "slug": "gaufres",
          "title": "Gaufres",
          "updatedAt": "2023-11-18T17:42:16.885Z"
        },
        "recipesId": "6a19f20d-d0a2-4f14-8a04-9e4215eb0fed"
      },
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "recipe": {
          "createdAt": "2023-05-07T15:24:01.328Z",
          "id": "d97dd78d-e708-4282-840a-1543d40899db",
          "servings": 10,
          "servingsUnit": "pancakes",
          "slug": "pancakes",
          "title": "Pancake",
          "updatedAt": "2023-11-18T17:42:24.082Z"
        },
        "recipesId": "d97dd78d-e708-4282-840a-1543d40899db"
      },
      {
        "categoriesId": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
        "recipe": {
          "createdAt": "2023-04-09T16:51:49.226Z",
          "id": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407",
          "servings": 6,
          "servingsUnit": "Personnes",
          "slug": "visitandines",
          "title": "Visitandines",
          "updatedAt": "2023-11-18T17:42:39.352Z"
        },
        "recipesId": "f1f2e7ea-edb2-42ff-8ef4-ccc3731fe407"
      }
    ],
    "createdAt": "2023-04-07T14:07:42.078Z",
    "id": "0f9a2c6b-1a76-4772-9abe-3eed2dd055b3",
    "name": "Dessert",
    "updatedAt": "2023-04-07T14:07:42.078Z"
  },
  "layout": "category",
  "title": "Dessert"
}
---
