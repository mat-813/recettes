---json
{
  "category": {
    "categoriesRecipes": [
      {
        "categoriesId": "8b0e03d3-8528-4244-86bd-29252531ce69",
        "recipe": {
          "createdAt": "2023-04-02T20:10:02.330Z",
          "id": "7f3773bb-f220-475a-bcf0-f09bb6b43aab",
          "servings": 2,
          "servingsUnit": "parts",
          "slug": "butter-tofu",
          "title": "Butter Tofu",
          "updatedAt": "2023-11-18T17:38:24.338Z"
        },
        "recipesId": "7f3773bb-f220-475a-bcf0-f09bb6b43aab"
      },
      {
        "categoriesId": "8b0e03d3-8528-4244-86bd-29252531ce69",
        "recipe": {
          "createdAt": "2023-04-09T16:13:48.821Z",
          "id": "6c69abcd-550e-494f-af4a-04b74e59d5c0",
          "servings": 6,
          "servingsUnit": "personnes",
          "slug": "galettes-au-sarrasin",
          "title": "Galettes au sarrasin",
          "updatedAt": "2023-11-18T17:42:10.895Z"
        },
        "recipesId": "6c69abcd-550e-494f-af4a-04b74e59d5c0"
      },
      {
        "categoriesId": "8b0e03d3-8528-4244-86bd-29252531ce69",
        "recipe": {
          "createdAt": "2023-11-18T12:03:45.163Z",
          "id": "3ff395ea-2139-4d23-8de2-39fa538c7193",
          "servings": 3,
          "servingsUnit": "personnes",
          "slug": "potimarron-farci",
          "title": "Potimarron farci",
          "updatedAt": "2023-11-18T17:42:35.322Z"
        },
        "recipesId": "3ff395ea-2139-4d23-8de2-39fa538c7193"
      }
    ],
    "createdAt": "2023-04-02T19:35:05.247Z",
    "id": "8b0e03d3-8528-4244-86bd-29252531ce69",
    "name": "Plat",
    "updatedAt": "2023-04-02T19:35:05.247Z"
  },
  "layout": "category",
  "title": "Plat"
}
---
