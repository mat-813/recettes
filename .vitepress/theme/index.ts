// https://vitepress.dev/guide/custom-theme
import './style.scss';

import Layout from './MyLayout.vue';

export default {
  Layout,

  enhanceApp({ _app, _router, _siteData }) {
    // ...
  },
};
