// eslint-disable-next-line import/no-extraneous-dependencies
import { createContentLoader } from 'vitepress';

export default createContentLoader('recipes/*.md');
