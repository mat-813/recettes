// eslint-disable-next-line import/no-extraneous-dependencies
import { defineConfig } from 'vitepress';

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'Les recettes de Mat',
  description: 'Mes petites recettes de cuisine à moi',
  outDir: './public',
  lang: 'fr-FR',

  sitemap: {
    hostname: 'https://recette.mat.cc',
    lastmodDateOnly: false,
  },

  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Examples', link: '/markdown-examples' },
    ],

    sidebar: [
      {
        text: 'Examples',

        items: [
          { text: 'Markdown Examples', link: '/markdown-examples' },
          { text: 'Runtime API Examples', link: '/api-examples' },
        ],
      },
    ],

    socialLinks: [{ icon: 'github', link: 'https://github.com/vuejs/vitepress' }],
  },
});
